<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src='<?php bloginfo('template_directory'); ?>/sidr/jquery.sidr.min.js'></script>

<?php wp_head(); ?>

<!-- REMOVE ME!!!!!!! -->
<!-- livereload -->
	<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
	</script>

</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
	
	<!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a> -->
	
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<div class="container">
				<div class="row">
					<div class="col-xs-5 col-md-8 col-md-push-4">
						<div id="sidr" class="main-navigation hidden-md hidden-lg" role="navigation">
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						</div><!-- #site-navigation -->
						<nav id="site-navigation" class="main-navigation hidden-xs hidden-sm" role="navigation">
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						</nav><!-- #site-navigation -->
						<nav id="site-navigation" class="main-navigation  visible-xs visible-sm" role="navigation">
							<a id="sidr-menu" class="sidr-toggle" href="#sidr"><i class="fa fa-bars"></i></a>
						</nav><!-- #site-navigation -->
					</div>
					<div class="col-xs-7 col-md-4 col-md-pull-8">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="responsive" src="<?php bloginfo('template_directory'); ?>/img/logo.png"></a>		
					</div>
				</div>
			</div>
		</div><!-- .site-branding -->		
	</header><!-- #masthead -->

	<script>
		$(document).ready(function($) {
		  $('#sidr-menu').sidr();
		});
	</script>

	<div id="content" class="site-content">