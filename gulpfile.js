var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	image = require('gulp-image'),
	notify = require('gulp-notify'),
	livereload = require('gulp-livereload'),
	del = require('del');

gulp.task('sass', function() {
	return sass('sass/style.scss', { style: 'expanded' })
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4' ))
		.pipe(gulp.dest(''))
		.pipe(notify({ message: 'Styles task complete' }))
		.pipe(livereload());
});

gulp.task('image', function () {
  gulp.src('library/images/src/*')
    .pipe(image())
    .pipe(gulp.dest('library/images'));
});

gulp.task('clean', function(cb) {
	del(['style.css'], cb)
});

gulp.task('watch', function(){
	var server = livereload();
	livereload.listen();
	gulp.watch('sass/**/*.scss', ['sass']);
	gulp.watch('images/src/**/*', ['image']);
	gulp.watch(['*']).on('change', livereload.changed);
});

gulp.task('default', ['clean'], function() {
	gulp.start('sass', 'image');
});