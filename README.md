Basic instructions on using the LE-Bedrock theme

1. Start with variables.scss
2. base.twig


Best practices for CSS
----------------------

Navbar & Logo
- Logo file same height as navbar (inc.margins)
- navbar height = logo height = line height (1rem = 16px)
- If not possible then set logo line-height to above

Centering an image of fixed width block
- block width = eg. 40px
- .block {position:absolute; left:50%; margin-left:-20px (this is half the block width) }

Vettical aligning in BS - Use .vcenter class in base.scss - ALSO Notice <!-- --> in below markup
- <div class="row">
		<div class="col-md-6 vcenter">
			<p>left</p>
		</div><!--
		--><div class="col-md-6 vcenter">
			<p>right</p>
		</div>
	</div>