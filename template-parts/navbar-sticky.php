<nav id="site-navigation" class="sticker main-navigation hidden-sm hidden-xs">
	<div class="container">
		<div class="row">
			<div>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</div>
		</div>
	</div><!-- container -->
</nav><!-- #site-navigation -->

<script>
	jQuery(document).ready(function($) {	  
	  jQuery('.sticker').stick_in_parent();
	});
</script>