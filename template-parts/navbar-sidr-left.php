<div id="sidr">
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_id' => 'mobile-menu' ) ); ?>
	</nav>
</div>

<script>
jQuery(document).ready(function() {
  jQuery('#sidr-toggle').sidr();
});
</script>