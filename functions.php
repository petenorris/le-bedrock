<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'le_scripts' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu('primary');
		$context['mobile'] = new TimberMenu('mobile');
		$context['footer'] = new TimberMenu('footer');
		$context['social'] = new TimberMenu('social');
		$context['site'] = $this;
		return $context;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( 'myfoo', new Twig_Filter_Function( 'myfoo' ) );
		return $twig;
	}

  function le_scripts() {
		wp_enqueue_style( 'style', get_stylesheet_uri() );
		wp_enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300', false );
		wp_enqueue_style( 'fontawesome', 'http:////maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', false );
		wp_enqueue_script( 'cubeport', get_template_directory_uri() . '/cubeportfolio/js/jquery.cubeportfolio.min.js');
		wp_enqueue_script( 'parallax', get_template_directory_uri() . '/js/parallax.min.js');
		wp_enqueue_script( 'sticky', get_template_directory_uri() . '/js/sticky-kit.js');
		wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/slick/slick.min.js');
		wp_enqueue_script( 'sidr', get_template_directory_uri() . '/sidr/jquery.sidr.min.js');
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

}

new StarterSite();

function myfoo( $text ) {
	$text .= ' bar!';
	return $text;
}



// ALL BELOW ARE TAKEN FROM PRE TIMBER FUNCTIONS.PHP - TO BE INTEGRATED WITH ABOVE

// ACF OPTIONS PAGE
if(function_exists('acf_add_options_page')) {
	acf_add_options_page();
	acf_add_options_sub_page('Header');
}


// CUSTOM POST TYPE
// function custom_post_type() {
//     $args = array(
//       'public' => true,
//       'label'  => 'Books'
//     );
//     register_post_type( 'book', $args );
// }
// add_action( 'init', 'custom_post_type' );


// SETUP
if ( ! function_exists( 'le_setup' ) ) :

function le_setup() {
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
		'mobile' => esc_html__( 'Mobile Menu', '_s' ),
		'footer' => esc_html__( 'Footer Menu', '_s' ),
		'social' => esc_html__( 'Social Menu', '_s' ),
	) );
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// WP BACKGROUND IMAGE
	// add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );
	define( 'NO_HEADER_TEXT', true );
}
endif; // le_setup
add_action( 'after_setup_theme', 'le_setup' );




// REGISTER WIDGETS
function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );


/**
 * Implement the Custom Header feature.
 */


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
