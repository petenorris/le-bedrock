<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Fontawesome and Sidr -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- Uncomment of cube portfolio is required - check functions.php too -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/cubeportfolio/js/jquery.cubeportfolio.min.js"></script> -->

<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<!-- <script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website. Please view our privacy and cookie policy for more details","dismiss":"Got it!","learnMore":"More info","theme":"dark-top"};
</script>
<script type="text/javascript" src="//s3.amazonaws.com/cc.silktide.com/cookieconsent.latest.min.js"></script> -->
<!-- End Cookie Consent plugin -->
<?php wp_head(); ?>
<!-- Google analytics code -->
<?php the_field('google_tracking_code', 'option'); ?>

<!-- REMOVE ME!!!!!!! -->
<!-- livereload -->
	<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
	</script>

</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
	
		<!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a> -->
		<a id="sidr-toggle" class="sidr-toggle visible-xs visible-sm" href="#sidr"><i class="fa fa-bars"></i></a>

		<header id="masthead" class="site-header">
			<div class="site-branding">
				<div class="container">			
					<!-- <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> -->
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt=""></a>
					<p class="site-description"><?php bloginfo( 'description' ); ?></p>
				</div>
			</div><!-- .site-branding -->			
		</header><!-- #masthead -->

		<?php get_template_part( 'template-parts/navbar', 'sticky' ); ?>
		<?php get_template_part( 'template-parts/navbar', 'sidr-left' ); ?>
		
	<div id="content" class="site-content">